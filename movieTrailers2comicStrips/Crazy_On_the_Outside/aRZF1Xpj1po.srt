﻿1
00:00:08,000 --> 00:00:10,349
after three years in prison

2
00:00:10,349 --> 00:00:13,170
Tommy Zelda couldn't wait to get home

3
00:00:13,170 --> 00:00:15,630
you're my brother I love you so I lied

4
00:00:15,630 --> 00:00:18,480
until he got there so where have I been

5
00:00:18,480 --> 00:00:23,510
for three years welcome home from France

6
00:00:23,510 --> 00:00:26,160
statistically speaking people who go to

7
00:00:26,160 --> 00:00:27,900
France and then get out of France

8
00:00:27,900 --> 00:00:30,000
usually end up back in France

9
00:00:30,000 --> 00:00:32,790
his old partner is tempting him mocking

10
00:00:32,790 --> 00:00:36,120
in Viki means me Tommy I want you back

11
00:00:36,120 --> 00:00:38,309
that take his twelve million it just got

12
00:00:38,309 --> 00:00:39,989
out of prison and how's that going for

13
00:00:39,989 --> 00:00:44,360
you know a little mess on the poop deck

14
00:00:44,360 --> 00:00:49,410
his ex is using him I marry Frank with

15
00:00:49,410 --> 00:00:51,120
you and I are gonna see each other

16
00:00:51,120 --> 00:00:52,289
whenever we can

17
00:00:52,289 --> 00:00:55,649
brendi oh hey how come you didn't answer

18
00:00:55,649 --> 00:00:58,320
I was just watched Gemma the shower I

19
00:00:58,320 --> 00:01:03,270
didn't hear the sound of the water his

20
00:01:03,270 --> 00:01:05,909
sister is smothering him I want to start

21
00:01:05,909 --> 00:01:07,950
by saying I never liked Christie six

22
00:01:07,950 --> 00:01:10,049
months after you went in she dumped you

23
00:01:10,049 --> 00:01:12,150
true me a lot of letters that was me

24
00:01:12,150 --> 00:01:14,640
what about the sexual stuff yeah that

25
00:01:14,640 --> 00:01:17,040
was a little achy Wow everyone in his

26
00:01:17,040 --> 00:01:20,189
life is driving him crazy hey hey it'll

27
00:01:20,189 --> 00:01:22,350
pass he'll do anything where's the

28
00:01:22,350 --> 00:01:25,990
probation department dumb Oh

29
00:01:25,990 --> 00:01:29,090
well to turn his life around you're not

30
00:01:29,090 --> 00:01:30,770
quite what I expected when the word

31
00:01:30,770 --> 00:01:33,110
probation officer popped up come on hey

32
00:01:33,110 --> 00:01:37,670
buddy Tim Allen Sigourney Weaver ray

33
00:01:37,670 --> 00:01:40,130
liotta Jeanne Tripplehorn Becky Simmons

34
00:01:40,130 --> 00:01:47,600
Julie Bowen and Kelsey Grammer in a

35
00:01:47,600 --> 00:01:50,000
comedy about trying to go straight

36
00:01:50,000 --> 00:01:52,400
did you ever tell anybody I'm thinking

37
00:01:52,400 --> 00:01:54,860
about it while trying not to go I'm not

38
00:01:54,860 --> 00:01:57,170
the guy that went into prison I'm the

39
00:01:57,170 --> 00:02:00,490
man that came out

40
00:02:00,490 --> 00:02:04,850
crazy on the outside so sweet fairies

41
00:02:04,850 --> 00:02:06,440
please what are you saying what's she

42
00:02:06,440 --> 00:02:08,869
saying French boy didn't you pick up any

43
00:02:08,869 --> 00:02:12,060
French

44
00:02:12,060 --> 00:02:14,120
you

